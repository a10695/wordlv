﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="21008000">
	<Property Name="NI.Lib.ContainingLib" Type="Str">WordLV.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../WordLV.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91M&lt;/W-,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@P_W`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"&lt;Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"O[^NF!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">553680896</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.2</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"9@5F.31QU+!!.-6E.$4%*76Q!!%;Q!!!2T!!!!)!!!%9Q!!!!A!!!!!AR8&lt;X*E4&amp;9O&lt;(:M;7)/6W^S:%R7,GRW9WRB=X-!!!#A)1#!!!!Q!!!I!!!!!!!!"!!$!$Q!P!!@1)!#!!!!!!%!!1!'`````Q!!!!!!!!!!!!!!!&amp;TTNR#7E(2+AT/L..NV,[I!!!!-!!!!%!!!!!!ZM+QA6CZK4JPQ"C:MQ]V_V"W-W9]!MA4JA!G9\0B#@A!!!!!!!!!!?Z.0&amp;8N0S5#VX"Z"":`+Q1%!!!$`````V"W-W9]!MA4JA!G9\0B#@A!!!"$48J!IA-?`2):G=&amp;[IVGT.!!!!"!!!!!!!!!!H!!&amp;-6E.$!!!!!1!#6EF-1A!!!!"16%AQ!!!!"1!"!!%!!!!!!A!$!!!!!!)!!1!!!!!!*Q!!!$"YH'0A97"K9,D!!-3-1-T5Q01$S0Y!YD-)=%"FE&amp;A!1W).L!!!!!"'!!!"'(C=9W$!"0_"!%AR-D#Q]!"J&amp;D2R-!VD5R0A-B?886"R:KA&lt;73'CD%!RZB&gt;!"B.)$KI'[A=7$C4PQ!%T&amp;L-"EOQG"1!!!!!!$!!"6EF%5Q!!!!!!!Q!!!6E!!!-A?*QL9'2A++]Q-R&amp;A9G"A"L)6'2I9EP.45LE9A(Q'"0D$R%!2;)#;JY5G&lt;HDA="I1[0(,NY$Z(MVP.$S&lt;@T#6#HLMA+HZ@]'D_9D'99`O2J$1=9=%M-*O2M.,LI9(`E_Y!.9'6-!P\Q$7XV"JT6$#&lt;HA!,(C]]1MDR"A-!U(K@4I&lt;)VA9A&amp;2H!JBKT)$Q#I#59T?(Y1&amp;(;U\_+1=/)\P0Y_!D&amp;E_Y96[^,%R?S3*-(JUO8!&lt;(8&lt;A#R%$+?E7YM.I8"D2!Q;0T(%*S&amp;S.%24=@1MSDUT%!B;?"T'O]A/)&lt;I-!(&gt;)%(E$"2C$N--':1Q&gt;L8^X;"YIE.3=S"!2*`$%SI7)_"E1(E?"#:#V6L!W1T1=6EI')A^FUI7Q.*TS]E]W&amp;CO]"K)0;R1M8Y'#&amp;]M*F!&gt;A#5L16E&lt;Y#SL9&amp;M!3D&lt;"]D_!(-P(NL:X]76!5O[2E\L!$4)=;A!!!!!!!!4!!!!#8C=9W"A9'2E!!)!!"1!!Q!!!!!/)1'!!1!!"D)R,D!O-1!!!!!!!!QB!)!!!!!%-D%O-!!!!!!/)1'!!1!!"D)R,D!O-1!!!!!!!!QB!)!!!!!%-D%O-!!!!!!/)1'!!1!!"D)R,D!O-1!!!!!!!"1"!!!!^6=VAHEGIIQO=V*/"EEZH1!!!!U!!!!!!!!!!!!!!!!!!!!!!!!!A0````_!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9"A!!'"G!!"BA9!!:A"A!'A!%!"M!$!!;Q$1!'D$M!"I06!!;!KQ!'A.5!"I#L!!;!V1!'A+M!"I$6!!:ALA!''.A!"A&lt;A!!9"A!!(`````!!!%!0```````````````````````````````````````````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!$U^!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!$WSB\+S01!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!$WSBW*C9G+SMDU!!!!!!!!!!!!!!!!!!!!!``]!!$WSBW*C9G*C9G*CML)^!!!!!!!!!!!!!!!!!!$``Q#SBW*C9G*C9G*C9G*C9L+S!!!!!!!!!!!!!!!!!0``!)?(9G*C9G*C9G*C9G*C`L)!!!!!!!!!!!!!!!!!``]!B\+SBW*C9G*C9G*C`P\_BQ!!!!!!!!!!!!!!!!$``Q#(ML+SMI&gt;C9G*C`P\_`P[(!!!!!!!!!!!!!!!!!0``!)?SML+SML+(MP\_`P\_`I=!!!!!!!!!!!!!!!!!``]!B\+SML+SML,_`P\_`P\_BQ!!!!!!!!!!!!!!!!$``Q#(ML+SML+SMP\_`P\_`P[(!!!!!!!!!!!!!!!!!0``!)?SML+SML+S`P\_`P\_`I=!!!!!!!!!!!!!!!!!``]!B\+SML+SML,_`P\_`P\_BQ!!!!!!!!!!!!!!!!$``Q#(ML+SML+SMP\_`P\_`P[(!!!!!!!!!!!!!!!!!0``!,+SML+SML+S`P\_`P\_ML)!!!!!!!!!!!!!!!!!``]!!)?(ML+SML,_`P\_ML+(!!!!!!!!!!!!!!!!!!$``Q!!!!#(ML+SMP\_ML+(!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!B\+SML*C!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!)&gt;C!!!!!!!!!!!!!!!!!!!!!!!!!!$```````````````````````````````````````````]!!!!-!!&amp;'5%B1!!!!!!!$!!!!"!!!!!!!!!2T!!!,XHC=L::04"R6(-&gt;`&lt;VDQ,98U$&lt;#&amp;6?A/SSQW6:,[J\:&amp;V#+$7C'VM"$2L)6.&gt;WGLU'X:B2KD=*EUY&gt;#$EJ"IUCN8$ZBY[5EW8C:'$IW(^L#7'"/DV8JI.*(:]@&gt;G&gt;W:W^M`1;#&amp;-*OTP_X[`^`V_XMQ#\,P./I1],/N!W!/]'&gt;7B-;%2A'Q@B?*0\T6AE_1@)'V"IM.*/MHO#(H3K5.41OOF2Z26O)`6RI`':`!6G7:XM&lt;3""8'R2BXW*\2W]5UZR_1P/_86?GN6%&lt;L9&gt;:)8JO418`3[GM+'I"\C6\'0Z)%IX4[@'DY4PZ25:@Z@@R].GEP[&gt;7#+VLQAZZ\%&amp;&lt;(V._;3Z#:ME?_M*1'80!3&lt;GZO/3#S)?MURDH0.&amp;CZ\C^TUU,3A*CXHHD9VD;9'_^SS_CCB`"K@H9P+J;WSBEK5@7DN?-NKJ94UE[:M:W=(:8ANSC\IU#&lt;H8K6"?D&gt;RGT30K6]$!;*OIXAG/UW.&lt;?.48#5GHO:2G)I74#034VA5\[-[(&amp;9V91:]6M&gt;&amp;7$/T]&amp;F:$'!7Z$5T#\_SSM!*9WW0-"2@.NQV.,?9TC18J.3M&gt;'YOHEZ,FR=O,M5T33E2T]1L9XJ*U@T(O!?]G5E)"-!(6W#KV0)5&lt;'RMI!VY&gt;;1PI\2&gt;TNE[U&gt;R.T,9^Y&gt;D/OTL_P9,_+8^0H/=?2PI&amp;.\BD$LD0)LAC+R`,"?\22Q`O#YL7N6Q',ER!$*)?%"YLC%L!D;&amp;K(C9].-&gt;2M_)#.Y:^ZKU_8O#?5$272G\-[G74O\[_\N+B]`UWO4Z#,(+.87/8EXP0_!/OQHQ*O5DLG%UL@AIQAFV=N*[N37P%/Q'J!1L%.AQP*3^FUJ6"4&amp;?CW9Y-4-':0&gt;'=+5/TV7V1&lt;44D;&amp;$E(9C]#Q[A&gt;7Z!5Q[A#2?AVH!O1'=@0;$HKQ%[#I0QHA&gt;M&amp;SI!(546/)R[;#Z7!$K)@=;N0F[!PF]*[+$6SQP1$WT`;W*[X\A(9T"&gt;AOEEBK*I%G@0_-89B=0QBBH#9V9)6[J#3D#&amp;TDUA&amp;5R)V8!UE\J=G=3#IN&amp;HZ"T":9P:NE%&gt;0!]^VE:`/FLJ;LIACDOCA^!$!T"5+YF^/G1+2](VK%5`3\U81\BDA17,S@T'R96\A^'A0_1-M+BIQ2@&gt;Z_I!V/-%LVND'W+N8*=5L?/%H!M[QYP'TX!%'R?(`\WK%J/^7H+SGM]64J;0HST_7HR,BY_1G)(3?:[#R^W0GI`Z52IR5QQ55T4_2#-%?/)BTF+6"*NU_!3^9()O9$57],=67H%,"U&gt;Q%8YVD@6R-_OE&lt;-A[PK)KG38&gt;`-L#2&gt;@H;,DA?XFJQO#F?.W\^0_P;I?.'VT'$4YHZ^YW0&gt;NP0ZY$%,"DANGK?_1FEFH3T5PM&amp;MH+;6SFRM/8@P\@3_U^)FIL*7C.NR81KE?ULJXF0N%I6J]O/6TUTL?,V0A6HS@Y6_UT[RMVD=K+Y68!BH'!&amp;2U#SF$,^_Q5?NB%4^&amp;B&gt;A.@$`CG/%"PU!@:(_TPX.F27[FOE[41)X]2IBX`!DC$5;A!!!!!"!!!!%%!!!!%!!!!!!!!!!Q!!5*%3&amp;!!!!!!!!-!!!!%!!!!!!!!!')!!!"S?*RD9'$)%Z"A_M&gt;1^Z?"3?!LE#(^FY&amp;:U)`R.Q-$JZ`!93$.+#!*&amp;*&lt;^S]!OK!U7VD[CS]%!";JMD"S3()=&amp;/=!S(#U;$0```_@Y?O1;8-52(TB4::9]BQ1!&amp;'):!!!!!!!!"!!!!!=!!!*S!!!!"Q!!!#&amp;@&lt;GF@4'&amp;T&gt;%NO&lt;X&gt;O4X&gt;O;7ZH4&amp;:$&lt;'&amp;T=U.M&gt;8.U:8)!!!!Y)1#!!!!!!!%!#!!Q`````Q!"!!!!!!!=!!!!!1!51&amp;!!!!V8&lt;X*E4&amp;9[6W^S:%R7!!%!!!!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'&amp;C4X*E:8)!!!!N)1#!!!!!!!)!"1!(!!!-!%!!!@````]!!!!"!!%!!!!#!!!!!!!!!!%!!!!!!!!!'UR71WRB=X.1=GFW982F2'&amp;U962J&lt;76T&gt;'&amp;N=!!!!"EB!)!!!!!!!1!&amp;!!=!!!%!!.[M0LM!!!!!!!!!*ER71WRB=X.1=GFW982F2'&amp;U95RB=X2"=("M;76E6'FN:8.U97VQ!!!!'3%!A!!!!!!"!!5!"Q!!!1!!XKQ_OQ!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6(FQ:52F=W-!!!"E)1#!!!!!!!%!#!!Q`````Q!"!!!!!!")!!!!"!!+1#%%5X2P=!!!%%"Q!"E!!1!!"&amp;.U&lt;X!!!""!5!!"!!%'28:F&lt;H2T!!!71&amp;!!!1!#$6&gt;P=G2-6DJ8&lt;X*E4&amp;9!!1!$!!!!!!!!!"Z-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U96.J?G5!!!!:)1#!!!!!!!%!"1!$!!!"!!!!!!!%!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U91!!!&amp;1B!)!!!!!!"!!+1#%%5X2P=!!!%%"Q!"E!!1!!"&amp;.U&lt;X!!!""!5!!"!!%'28:F&lt;H2T!!!71&amp;!!!1!#$6&gt;P=G2-6DJ8&lt;X*E4&amp;9!!1!$!!!!!!!!!!!!!!!!!!1!"1!/!!!!"!!!!(]!!!!I!!!!!A!!"!!!!!!#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!2A!!!']?*S.5,N/QU!1(0O-YRA4%JY/%J,45+3AI5?7%&amp;15%5AA/IQ@+.+*M_S,2=E(]1@]#``!(T!_GV$1I.'N&gt;H&lt;P:G=0Q"(]?/&lt;=;F5#Y\D%&amp;";QZAMSS\VM]B&gt;&gt;!Q`Y_HQ``Q!AAHN6:&gt;&gt;XJ\+2S[@2GK1SK7O`J[G7G"M*_`B#LGK&gt;6Z%K)H-H+KNFE_A]SB+&gt;9)/!DYT#)2Q3U6/=Q=5Q(E!5]BF?`#&lt;5D?Y+O3K-11%01`BCF26]/O/\&lt;IG!^8&lt;W&amp;A,D934U;]&lt;O!D9WM9W4@TKTQ?#WA2`DY:'G(&amp;R2*M)/&gt;JGXW//.(TA^`F:_/`M'X9I7$JCF;.5^&gt;E&amp;&gt;Q8%$D$(")5*;#!G,:]L+B*R7PA(]*%$`!!!!:1!"!!)!!Q!%!!!!3!!0"!!!!!!0!.E!V!!!!&amp;%!$Q1!!!!!$Q$:!.1!!!";!!]%!!!!!!]!W1$5!!!!9Y!!B!#!!!!0!.E!V!B4:7&gt;P:3"631B4:7&gt;P:3"631B4:7&gt;P:3"631%Q!!!!5F.31QU+!!.-6E.$4%*76Q!!%;Q!!!2T!!!!)!!!%9Q!!!!!!!!!!!!!!#!!!!!U!!!%;!!!!"Z-35*/!!!!!!!!!8B-6F.3!!!!!!!!!9R36&amp;.(!!!!!!!!!;"$1V.5!!!!!!!!!&lt;2-38:J!!!!!!!!!=B$4UZ1!!!!!!!!!&gt;R544AQ!!!!!!!!!@"%2E24!!!!!!!!!A2-372T!!!!!!!!!BB735.%!!!!!!!!!CR(1U2*!!!!!!!!!E"W:8*T!!!!"!!!!F241V.3!!!!!!!!!LB(1V"3!!!!!!!!!MR*1U^/!!!!!!!!!O"J9WQY!!!!!!!!!P2-37:Q!!!!!!!!!QB'5%6Y!!!!!!!!!RR'5%BC!!!!!!!!!T"'5&amp;.&amp;!!!!!!!!!U275%21!!!!!!!!!VB-37*E!!!!!!!!!WR#2%6Y!!!!!!!!!Y"#2%BC!!!!!!!!!Z2#2&amp;.&amp;!!!!!!!!![B73624!!!!!!!!!\R%6%B1!!!!!!!!!^".65F%!!!!!!!!!_2)36.5!!!!!!!!!`B71V21!!!!!!!!"!R'6%&amp;#!!!!!!!!"#!!!!!!`````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#1!!!!!!!!!!0````]!!!!!!!!!S!!!!!!!!!!!`````Q!!!!!!!!$=!!!!!!!!!!$`````!!!!!!!!!/1!!!!!!!!!!0````]!!!!!!!!"%!!!!!!!!!!!`````Q!!!!!!!!%9!!!!!!!!!!$`````!!!!!!!!!51!!!!!!!!!!0````]!!!!!!!!"E!!!!!!!!!!!`````Q!!!!!!!!'A!!!!!!!!!!$`````!!!!!!!!!Q!!!!!!!!!!"0````]!!!!!!!!$'!!!!!!!!!!(`````Q!!!!!!!!-M!!!!!!!!!!D`````!!!!!!!!!TQ!!!!!!!!!#@````]!!!!!!!!$5!!!!!!!!!!+`````Q!!!!!!!!.A!!!!!!!!!!$`````!!!!!!!!!X1!!!!!!!!!!0````]!!!!!!!!$D!!!!!!!!!!!`````Q!!!!!!!!/A!!!!!!!!!!$`````!!!!!!!!"#1!!!!!!!!!!0````]!!!!!!!!)+!!!!!!!!!!!`````Q!!!!!!!!AY!!!!!!!!!!$`````!!!!!!!!#%!!!!!!!!!!!0````]!!!!!!!!-O!!!!!!!!!!!`````Q!!!!!!!!T!!!!!!!!!!!$`````!!!!!!!!$-A!!!!!!!!!!0````]!!!!!!!!-W!!!!!!!!!!!`````Q!!!!!!!!TA!!!!!!!!!!$`````!!!!!!!!$5A!!!!!!!!!!0````]!!!!!!!!.5!!!!!!!!!!!`````Q!!!!!!!!`)!!!!!!!!!!$`````!!!!!!!!$^!!!!!!!!!!!0````]!!!!!!!!0W!!!!!!!!!!!`````Q!!!!!!!"!%!!!!!!!!!)$`````!!!!!!!!%3!!!!!!#F&gt;P=G2-6CZD&gt;'Q!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!AR8&lt;X*E4&amp;9O&lt;(:M;7)/6W^S:%R7,GRW9WRB=X.16%AQ!!!!!!!!!!!!!!!%!!%!!!!!!!!"!!!!!1!E1&amp;!!!"R8&lt;X*E4&amp;9[6W^S:%R7,GRW9WRB=X.@-4!Q.TAQ!!!"!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!)1#!!!!!!!!!!!(``Q!!!!%!!!!!!!!!!!!!!1!E1&amp;!!!"R8&lt;X*E4&amp;9[6W^S:%R7,GRW9WRB=X.@-4!Q.TAQ!!!"!!!!!!!"`````A!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!#%!A!!!!!!!!!!"``]!!!!"!!!!!!!"!!!!!!%!*%"1!!!=6W^S:%R7/F&gt;P=G2-6CZM&gt;G.M98.T8T%Q-$=Y-!!!!1!!!!!!!@````Y!!!!!!!!#&amp;5&amp;D&gt;'^S)%:S97VF&gt;W^S;SZM&gt;GRJ9AV"9X2P=CZM&gt;G.M98.T5&amp;2)-!!!!!!!!!!!!#%!A!!!!!!!!!!!!!!!!!!"!!!!!!!#!!!!!!1!#E!B"&amp;.U&lt;X!!!""!=!!:!!%!!!24&gt;'^Q!!!11&amp;!!!1!""E6W:7ZU=Q!!71$RXKQ_OQ!!!!--6W^S:%R7,GRW&lt;'FC$F&gt;P=G2-6CZM&gt;G.M98.T#F&gt;P=G2-6CZD&gt;'Q!+E"1!!%!!BV$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!-!!!!"`````Q!!!!!!!!!#&amp;5&amp;D&gt;'^S)%:S97VF&gt;W^S;SZM&gt;GRJ9AV"9X2P=CZM&gt;G.M98.T5&amp;2)-!!!!!!!!!!!!#%!A!!!!!!!!!!!!!!"!!!!+F&gt;P=G2-6CZM&gt;GRJ9DJ8&lt;X*E4&amp;9O&lt;(:D&lt;'&amp;T=V]R-$!X/$!O&lt;(:D&lt;'&amp;T=Q</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.1</Property>
	<Item Name="Parent Libraries" Type="Parent Libraries">
		<Item Name="Actor Framework.lvlib:Actor.lvclass" Type="Parent" URL="/&lt;vilib&gt;/ActorFramework/Actor/Actor.lvclass"/>
	</Item>
	<Item Name="WordLV.ctl" Type="Class Private Data" URL="WordLV.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="AF Override VIs" Type="Folder">
		<Item Name="Actor Core.vi" Type="VI" URL="../Actor Core.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%&lt;!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$B!=!!?!!!F&amp;5&amp;D&gt;'^S)%:S97VF&gt;W^S;SZM&gt;GRJ9AV"9X2P=CZM&gt;G.M98.T!!F"9X2P=C"P&gt;81!&amp;E"1!!-!!!!"!!)):8*S&lt;X)A;7Y!!$"!=!!?!!!&gt;$&amp;&gt;P=G2-6CZM&gt;GRJ9AZ8&lt;X*E4&amp;9O&lt;(:D&lt;'&amp;T=Q!*6W^S:%R7)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!#1!!!!!!!!!!!!!!#1!!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#1!!!!!!%!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1090781568</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="Pre Launch Init.vi" Type="VI" URL="../Pre Launch Init.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$`!!!!#!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$*!=!!?!!!&gt;$&amp;&gt;P=G2-6CZM&gt;GRJ9AZ8&lt;X*E4&amp;9O&lt;(:D&lt;'&amp;T=Q!+6W^S:%R7)'^V&gt;!!!-%"Q!"Y!!"U-6W^S:%R7,GRW&lt;'FC$F&gt;P=G2-6CZM&gt;G.M98.T!!F8&lt;X*E4&amp;9A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!1!"!!%!!9$!!"Y!!!*!!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*)!!!!!!1!(!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">150995072</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">277356560</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="Stop Core.vi" Type="VI" URL="../Stop Core.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$:!!!!"1!%!!!!-E"Q!"Y!!"U-6W^S:%R7,GRW&lt;'FC$F&gt;P=G2-6CZM&gt;G.M98.T!!J8&lt;X*E4&amp;9A&lt;X6U!!!81!-!%':J&lt;G&amp;M)'6S=G^S)'.P:'5!!$"!=!!?!!!&gt;$&amp;&gt;P=G2-6CZM&gt;GRJ9AZ8&lt;X*E4&amp;9O&lt;(:D&lt;'&amp;T=Q!*6W^S:%R7)'FO!&amp;1!]!!-!!!!!!!!!!%!!!!!!!!!!!!!!!!!!A!$!Q!!?!!!!!!!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"!!!!#3!!!!!!%!"!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">150995072</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1351098896</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
	</Item>
	<Item Name="ctls" Type="Folder">
		<Item Name="LetterData.ctl" Type="VI" URL="../../ctls/LetterData.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$"!!!!!Q"=!0%!!!!!!!!!!AR8&lt;X*E4&amp;9O&lt;(:M;7)04'6U&gt;'6S5X2B&gt;'5O9X2M!$&gt;!&amp;A!%#%Z05V2"6&amp;64"5V"6%.)"EF/6U^32!F/4V2*4F&gt;05E1!#URF&gt;(2F=F.U982F!""!-0````]'4'6U&gt;'6S!!".!0%!!!!!!!!!!QR8&lt;X*E4&amp;9O&lt;(:M;7)/6W^S:%R7,GRW9WRB=X-/4'6U&gt;'6S2'&amp;U93ZD&gt;'Q!'E"1!!)!!!!"#ERF&gt;(2F=E2B&gt;'%!!!%!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">143654912</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
		</Item>
		<Item Name="LetterState.ctl" Type="VI" URL="../../ctls/LetterState.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"N!!!!!1"F!0%!!!!!!!!!!QR8&lt;X*E4&amp;9O&lt;(:M;7)/6W^S:%R7,GRW9WRB=X-04'6U&gt;'6S5X2B&gt;'5O9X2M!$&amp;!&amp;A!%#%Z05V2"6&amp;64"5V"6%.)"EF/6U^32!F/4V2*4F&gt;05E1!"%6O&gt;7U!!!%!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">135266304</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
		</Item>
		<Item Name="QueueData.ctl" Type="VI" URL="../../ctls/QueueData.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"G!!!!!Q!/1$$`````"6.U982F!!J!5Q2%982B!!"'!0%!!!!!!!!!!QR8&lt;X*E4&amp;9O&lt;(:M;7)/6W^S:%R7,GRW9WRB=X-.586F&gt;76%982B,G.U&lt;!!51&amp;!!!A!!!!%%2'&amp;U91!!!1!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">135266304</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
		</Item>
		<Item Name="WordData.ctl" Type="VI" URL="../../ctls/WordData.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%R!!!!"A!31$$`````#5:V&lt;'QA6W^S:!"=!0%!!!!!!!!!!AR8&lt;X*E4&amp;9O&lt;(:M;7)04'6U&gt;'6S5X2B&gt;'5O9X2M!$&gt;!&amp;A!%#%Z05V2"6&amp;64"5V"6%.)"EF/6U^32!F/4V2*4F&gt;05E1!#URF&gt;(2F=F.U982F!""!-0````]'4'6U&gt;'6S!!"%!0%!!!!!!!!!!AR8&lt;X*E4&amp;9O&lt;(:M;7)/4'6U&gt;'6S2'&amp;U93ZD&gt;'Q!)%"1!!)!!1!#%5RF&gt;(2F=C"%982B)%&amp;S=G&amp;Z!"Z!1!!"`````Q!$%5RF&gt;(2F=C"%982B)%&amp;S=G&amp;Z!%E!]1!!!!!!!!!$$&amp;&gt;P=G2-6CZM&gt;GRJ9AZ8&lt;X*E4&amp;9O&lt;(:D&lt;'&amp;T=QR8&lt;X*E2'&amp;U93ZD&gt;'Q!'%"1!!)!!!!%#6&gt;P=G1A2'&amp;U91!"!!5!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">143654912</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
		</Item>
	</Item>
	<Item Name="subvis" Type="Folder">
		<Item Name="AssignStringValue.vi" Type="VI" URL="../../AssignStringValue.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+\!!!!%Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!&amp;5!$!!Z(&gt;76T=S"$&lt;X6O&gt;%^V&gt;!!!%E!Q`````QF'&gt;7RM)&amp;&gt;P=G1!;Q$R!!!!!!!!!!--6W^S:%R7,GRW&lt;'FC$F&gt;P=G2-6CZM&gt;G.M98.T$URF&gt;(2F=F.U982F,G.U&lt;!!X1"9!"!B/4V.516265Q6.162$3!:*4F&gt;05E1*4E^535Z84V*%!!N-:82U:8*4&gt;'&amp;U:1!11$$`````"ERF&gt;(2F=A!!5Q$R!!!!!!!!!!--6W^S:%R7,GRW&lt;'FC$F&gt;P=G2-6CZM&gt;G.M98.T$ERF&gt;(2F=E2B&gt;'%O9X2M!#"!5!!#!!9!"R&amp;-:82U:8)A2'&amp;U93""=H*B?1!?1%!!!@````]!#"&amp;-:82U:8)A2'&amp;U93""=H*B?1"2!0%!!!!!!!!!!QR8&lt;X*E4&amp;9O&lt;(:M;7)/6W^S:%R7,GRW9WRB=X--6W^S:%2B&gt;'%O9X2M!#"!5!!#!!5!#2&amp;(&gt;76T=V&gt;P=G2%982B)%^V&gt;!!)!$$`````!"J!1!!#``````````]!#QF597*M:3"P&gt;81!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%5!$!!N(&gt;76T=S"$&lt;X6O&gt;!".!0%!!!!!!!!!!QR8&lt;X*E4&amp;9O&lt;(:M;7)/6W^S:%R7,GRW9WRB=X--6W^S:%2B&gt;'%O9X2M!"R!5!!#!!5!#1V(&gt;76T=V&gt;P=G2%982B!"J!1!!#``````````]!#QB597*M:3"J&lt;A!!6!$Q!!Q!!Q!%!!I!$!!.!!U!$1!.!!Y!$Q!1!"%#!!"Y!!!.#!!!$1E!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!)!!!!#!!!!1I!!!!!!1!3!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">134217728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1350574592</Property>
		</Item>
		<Item Name="ChangeCellColor.vi" Type="VI" URL="../../ChangeCellColor.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,(!!!!&amp;!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!&amp;5!$!!Z(&gt;76T=U.P&gt;7ZU)%^V&gt;!!!%E!Q`````QF'&gt;7RM)&amp;&gt;P=G1!;Q$R!!!!!!!!!!--6W^S:%R7,GRW&lt;'FC$F&gt;P=G2-6CZM&gt;G.M98.T$URF&gt;(2F=F.U982F,G.U&lt;!!X1"9!"!B/4V.516265Q6.162$3!:*4F&gt;05E1*4E^535Z84V*%!!N-:82U:8*4&gt;'&amp;U:1!11$$`````"ERF&gt;(2F=A!!5Q$R!!!!!!!!!!--6W^S:%R7,GRW&lt;'FC$F&gt;P=G2-6CZM&gt;G.M98.T$ERF&gt;(2F=E2B&gt;'%O9X2M!#"!5!!#!!9!"R&amp;-:82U:8)A2'&amp;U93""=H*B?1!?1%!!!@````]!#"&amp;-:82U:8)A2'&amp;U93""=H*B?1".!0%!!!!!!!!!!QR8&lt;X*E4&amp;9O&lt;(:M;7)/6W^S:%R7,GRW9WRB=X--6W^S:%2B&gt;'%O9X2M!"R!5!!#!!5!#1V8&lt;X*E)%2B&gt;'%A4X6U!!A!-0````]!%!"!!!,``````````Q!,!"R!=!!)!!%!$!!.!!!.6'&amp;C&lt;'5A5G6G)%^V&gt;!!%!!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!21!-!#E.V=H*F&lt;H23&lt;X=!!%E!]1!!!!!!!!!$$&amp;&gt;P=G2-6CZM&gt;GRJ9AZ8&lt;X*E4&amp;9O&lt;(:D&lt;'&amp;T=QR8&lt;X*E2'&amp;U93ZD&gt;'Q!'%"1!!)!"1!*#6&gt;P=G1A2'&amp;U91!=1(!!#!!"!!Q!$1!!$&amp;2B9GRF)&amp;*F:C"J&lt;A!!6!$Q!!Q!!Q!%!!I!$1!/!!Y!$A!/!!]!%!!2!")$!!"Y!!!.#!!!$1E!!!U+!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!)!!!!#!!!!!A!!!!!!1!4!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">134217728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
		</Item>
		<Item Name="CheckIfWin.vi" Type="VI" URL="../../CheckIfWin.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)9!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!R!)1:8;7ZO:8)!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%E!Q`````QF'&gt;7RM)&amp;&gt;P=G1!;Q$R!!!!!!!!!!--6W^S:%R7,GRW&lt;'FC$F&gt;P=G2-6CZM&gt;G.M98.T$URF&gt;(2F=F.U982F,G.U&lt;!!X1"9!"!B/4V.516265Q6.162$3!:*4F&gt;05E1*4E^535Z84V*%!!N-:82U:8*4&gt;'&amp;U:1!11$$`````"ERF&gt;(2F=A!!5Q$R!!!!!!!!!!--6W^S:%R7,GRW&lt;'FC$F&gt;P=G2-6CZM&gt;G.M98.T$ERF&gt;(2F=E2B&gt;'%O9X2M!#"!5!!#!!A!#2&amp;-:82U:8)A2'&amp;U93""=H*B?1!?1%!!!@````]!#B&amp;-:82U:8)A2'&amp;U93""=H*B?1"2!0%!!!!!!!!!!QR8&lt;X*E4&amp;9O&lt;(:M;7)/6W^S:%R7,GRW9WRB=X--6W^S:%2B&gt;'%O9X2M!#"!5!!#!!=!#R"(&gt;76T=V&gt;P=G2%982B4X6U!!"5!0!!$!!$!!1!"1!%!!1!"!!%!!1!"A!%!!Q!"!)!!(A!!!U)!!!!!!!!#1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!)!!!!!!!!!!!"!!U!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">134217728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1350574592</Property>
		</Item>
		<Item Name="CompareText.vi" Type="VI" URL="../../CompareText.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,J!!!!&amp;1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!%E!Q`````QF'&gt;7RM)&amp;&gt;P=G1!;Q$R!!!!!!!!!!--6W^S:%R7,GRW&lt;'FC$F&gt;P=G2-6CZM&gt;G.M98.T$URF&gt;(2F=F.U982F,G.U&lt;!!X1"9!"!B/4V.516265Q6.162$3!:*4F&gt;05E1*4E^535Z84V*%!!N-:82U:8*4&gt;'&amp;U:1!11$$`````"ERF&gt;(2F=A!!5Q$R!!!!!!!!!!--6W^S:%R7,GRW&lt;'FC$F&gt;P=G2-6CZM&gt;G.M98.T$ERF&gt;(2F=E2B&gt;'%O9X2M!#"!5!!#!!5!"B&amp;-:82U:8)A2'&amp;U93""=H*B?1!?1%!!!@````]!"R&amp;-:82U:8)A2'&amp;U93""=H*B?1"2!0%!!!!!!!!!!QR8&lt;X*E4&amp;9O&lt;(:M;7)/6W^S:%R7,GRW9WRB=X--6W^S:%2B&gt;'%O9X2M!#"!5!!#!!1!#""(&gt;76T=V&gt;P=G2%982B4X6U!!"4!0%!!!!!!!!!!QR8&lt;X*E4&amp;9O&lt;(:M;7)/6W^S:%R7,GRW9WRB=X--6W^S:%2B&gt;'%O9X2M!#*!5!!#!!1!#"*%:8.J=G6E6W^S:%2B&gt;'&amp;0&gt;81!!".!!Q!.2X6F=X.$&lt;X6O&gt;%^V&gt;!!%!!!!%E!B$5ZP&gt;%&amp;797RJ:&amp;&gt;P=G1!%5!$!!J(&gt;76T=U.P&gt;7ZU!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"2!-0````]+2X6F=X-A6W^S:!!!&amp;E"!!!(`````!"!)6W^S:%RJ=X1!!":!-0````]-2'6T;8*F:#"8&lt;X*E!!!51$$`````#E&gt;V:8.T)&amp;2F?(1!!&amp;1!]!!-!!-!#1!+!!M!$!!.!!Q!$A!0!"%!%A!4!Q!!?!!!$1A!!!E!!!!*!!!!$1=!!!!!!!!*!!!!!!!!!!I!!!!)!!!##!!!!1I!!!%+!!!!!!%!&amp;!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">134217728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
		<Item Name="ExactMatchCheck.vi" Type="VI" URL="../../ExactMatchCheck.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+?!!!!#Q!%!!!!%E!Q`````QF'&gt;7RM)&amp;&gt;P=G1!;Q$R!!!!!!!!!!--6W^S:%R7,GRW&lt;'FC$F&gt;P=G2-6CZM&gt;G.M98.T$URF&gt;(2F=F.U982F,G.U&lt;!!X1"9!"!B/4V.516265Q6.162$3!:*4F&gt;05E1*4E^535Z84V*%!!N-:82U:8*4&gt;'&amp;U:1!11$$`````"ERF&gt;(2F=A!!5Q$R!!!!!!!!!!--6W^S:%R7,GRW&lt;'FC$F&gt;P=G2-6CZM&gt;G.M98.T$ERF&gt;(2F=E2B&gt;'%O9X2M!#"!5!!#!!)!!R&amp;-:82U:8)A2'&amp;U93""=H*B?1!?1%!!!@````]!""&amp;-:82U:8)A2'&amp;U93""=H*B?1"2!0%!!!!!!!!!!QR8&lt;X*E4&amp;9O&lt;(:M;7)/6W^S:%R7,GRW9WRB=X--6W^S:%2B&gt;'%O9X2M!#"!5!!#!!%!"2"(&gt;76T=V&gt;P=G2%982B4X6U!!"4!0%!!!!!!!!!!QR8&lt;X*E4&amp;9O&lt;(:M;7)/6W^S:%R7,GRW9WRB=X--6W^S:%2B&gt;'%O9X2M!#*!5!!#!!%!"2*%:8.J=G6E6W^S:%2B&gt;'&amp;0&gt;81!!%U!]1!!!!!!!!!$$&amp;&gt;P=G2-6CZM&gt;GRJ9AZ8&lt;X*E4&amp;9O&lt;(:D&lt;'&amp;T=QR8&lt;X*E2'&amp;U93ZD&gt;'Q!(%"1!!)!!1!&amp;$5&gt;V:8.T6W^S:%2B&gt;'%!4Q$R!!!!!!!!!!--6W^S:%R7,GRW&lt;'FC$F&gt;P=G2-6CZM&gt;G.M98.T$&amp;&gt;P=G2%982B,G.U&lt;!!?1&amp;!!!A!"!!502'6T;8*F:&amp;&gt;P=G2%982B!&amp;1!]!!-!!!!"A!(!!!!!!!!!!!!!!!!!!A!#1!!!A!!?!!!!!!!!!U*!!!.#A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!I!!!!!!!!!!!%!#A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">134217728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1350574592</Property>
		</Item>
		<Item Name="Failure.vi" Type="VI" URL="../../Failure.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;/!!!!$Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!R!)1:'97FM:71!!"&amp;!!Q!+2X6F=X.F=U^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!81!-!%%&amp;M&lt;'^X97*M:5&gt;V:8.T:8-!!!V!!Q!(2X6F=X.F=Q!/1$$`````"6.U982F!!J!5Q2%982B!!!E!0%!!!!!!!!!!1V2&gt;76V:52B&gt;'%O9X2M!!Y!5!!#!!I!#Q!51(!!%A!"!!Q)586F&gt;75A;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!A!#1!.!Q!!?!!!$1A!!!!!!!!*!!!!$1I!!!!!!!!!!!!!!!!!!!!!!!!)!!!!#!!!!!A!!!!)!!!!!!%!$A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">134217728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
		</Item>
		<Item Name="FindOtherMatches.vi" Type="VI" URL="../../FindOtherMatches.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+?!!!!#Q!%!!!!%E!Q`````QF'&gt;7RM)&amp;&gt;P=G1!;Q$R!!!!!!!!!!--6W^S:%R7,GRW&lt;'FC$F&gt;P=G2-6CZM&gt;G.M98.T$URF&gt;(2F=F.U982F,G.U&lt;!!X1"9!"!B/4V.516265Q6.162$3!:*4F&gt;05E1*4E^535Z84V*%!!N-:82U:8*4&gt;'&amp;U:1!11$$`````"ERF&gt;(2F=A!!5Q$R!!!!!!!!!!--6W^S:%R7,GRW&lt;'FC$F&gt;P=G2-6CZM&gt;G.M98.T$ERF&gt;(2F=E2B&gt;'%O9X2M!#"!5!!#!!)!!R&amp;-:82U:8)A2'&amp;U93""=H*B?1!?1%!!!@````]!""&amp;-:82U:8)A2'&amp;U93""=H*B?1"2!0%!!!!!!!!!!QR8&lt;X*E4&amp;9O&lt;(:M;7)/6W^S:%R7,GRW9WRB=X--6W^S:%2B&gt;'%O9X2M!#"!5!!#!!%!"2"(&gt;76T=V&gt;P=G2%982B4X6U!!"4!0%!!!!!!!!!!QR8&lt;X*E4&amp;9O&lt;(:M;7)/6W^S:%R7,GRW9WRB=X--6W^S:%2B&gt;'%O9X2M!#*!5!!#!!%!"2*%:8.J=G6E6W^S:%2B&gt;'&amp;0&gt;81!!%U!]1!!!!!!!!!$$&amp;&gt;P=G2-6CZM&gt;GRJ9AZ8&lt;X*E4&amp;9O&lt;(:D&lt;'&amp;T=QR8&lt;X*E2'&amp;U93ZD&gt;'Q!(%"1!!)!!1!&amp;$5&gt;V:8.T6W^S:%2B&gt;'%!4Q$R!!!!!!!!!!--6W^S:%R7,GRW&lt;'FC$F&gt;P=G2-6CZM&gt;G.M98.T$&amp;&gt;P=G2%982B,G.U&lt;!!?1&amp;!!!A!"!!502'6T;8*F:&amp;&gt;P=G2%982B!&amp;1!]!!-!!!!"A!(!!!!!!!!!!!!!!!!!!A!#1!!!A!!?!!!!!!!!!U*!!!.#A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!I!!!!!!!!!!!%!#A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">134217728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1350574592</Property>
		</Item>
		<Item Name="generate word.vi" Type="VI" URL="../../subvis/generate word.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#?!!!!"1!%!!!!&amp;%!Q`````QNB:H2F=C"N982D;!!71%!!!@````]!!1F8&lt;X*E)%RJ=X1!&amp;%!Q`````QJ(&gt;76T=S"8&lt;X*E!!"5!0!!$!!!!!)!!!!$!!!!!!!!!!!!!!!!!!!!!!)!!(A!!!!!!!!*!!!!!!!!!!E!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"!!1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">134217728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
		</Item>
		<Item Name="string to char array.vi" Type="VI" URL="../../subvis/string to char array.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;X!!!!#!!31$$`````#5:V&lt;'QA6W^S:!"L!0%!!!!!!!!!!QR8&lt;X*E4&amp;9O&lt;(:M;7)/6W^S:%R7,GRW9WRB=X-04'6U&gt;'6S5X2B&gt;'5O9X2M!$&gt;!&amp;A!%#%Z05V2"6&amp;64"5V"6%.)"EF/6U^32!F/4V2*4F&gt;05E1!#URF&gt;(2F=F.U982F!""!-0````]'4'6U&gt;'6S!!"4!0%!!!!!!!!!!QR8&lt;X*E4&amp;9O&lt;(:M;7)/6W^S:%R7,GRW9WRB=X-/4'6U&gt;'6S2'&amp;U93ZD&gt;'Q!)%"1!!)!!1!#%5RF&gt;(2F=C"%982B)%&amp;S=G&amp;Z!"Z!1!!"`````Q!$%5RF&gt;(2F=C"%982B)%&amp;S=G&amp;Z!%E!]1!!!!!!!!!$$&amp;&gt;P=G2-6CZM&gt;GRJ9AZ8&lt;X*E4&amp;9O&lt;(:D&lt;'&amp;T=QR8&lt;X*E2'&amp;U93ZD&gt;'Q!'%"1!!)!!!!%#6&gt;P=G1A2'&amp;U91!11$$`````"F.U=GFO:Q!!'!$Q!!)!"1!'!A!!#!!!#1!!!AA!!!!!!1!(!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">134217728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1350574592</Property>
		</Item>
		<Item Name="VerifyWordGuess.vi" Type="VI" URL="../../VerifyWordGuess.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$-!!!!"Q!%!!!!'%!Q`````QZ(&gt;76T=V&gt;P=G24&gt;'&amp;U:1!!&amp;E!Q`````QR%:8.J=G6E)&amp;&gt;P=G1!!"2!-0````]+2X6F=X-A6'6Y&gt;!!!&amp;%!Q`````QJ(&gt;76T=S"8&lt;X*E!!!71%!!!@````]!"!B8&lt;X*E4'FT&gt;!!!6!$Q!!Q!!!!!!!%!!!!!!!!!!!!!!!!!!A!$!!5#!!"Y!!!!!!!!!!!!!!E!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!))!!!##!!!!AA!!!!!!1!'!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">134217728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
		</Item>
	</Item>
	<Item Name="Launcher.vi" Type="VI" URL="../Launcher.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"A!!!!!A!%!!!!6!$Q!!Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$!!"Y!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!"!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">134217728</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
	</Item>
	<Item Name="NameVI.vi" Type="VI" URL="../NameVI.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"O!!!!!Q!%!!!!$E!Q`````Q2/97VF!!"5!0!!$!!!!!!!!1!!!!!!!!!!!!!!!!!!!!!!!!)!!(A!!!!!!!!!!!!!#1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"!!)!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
	</Item>
	<Item Name="SetMenuBar.vi" Type="VI" URL="../SetMenuBar.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$,!!!!#!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!$E"Q!!E(476O&gt;3"J&lt;A"5!0!!$!!$!!1!"!!%!!1!"!!%!!1!"1!%!!1!"A-!!(A!!!U)!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!#!!!!!!"!!=!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
	<Item Name="WordLV FP.vi" Type="VI" URL="../../WordLV FP.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"A!!!!!A!%!!!!6!$Q!!Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$!!"Y!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!"!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1208254784</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
</LVClass>
